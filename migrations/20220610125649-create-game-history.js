"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("gameHistories", {
      hist_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      room_id: {
        type: Sequelize.INTEGER,
      },
      winner: {
        type: Sequelize.ARRAY(Sequelize.STRING),
      },
      session: {
        type: Sequelize.ARRAY(Sequelize.STRING),
      },
      is_active: {
        type: Sequelize.BOOLEAN,
      },
      playing_date: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("gameHistories");
  },
};
