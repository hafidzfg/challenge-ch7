"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.changeColumn("userGames", "user_id", {
      allowNull: false,
      unique: true,
      type: Sequelize.STRING,
      primaryKey: true,
    }),
      await queryInterface.changeColumn("userGames", "email", {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      }),
      await queryInterface.changeColumn("userGames", "password", {
        allowNull: false,
        type: Sequelize.STRING,
      });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.changeColumn("userGames", "user_id", {
      type: Sequelize.STRING,
      primaryKey: true,
    }),
      await queryInterface.changeColumn("userGames", "email", {
        type: Sequelize.STRING,
      }),
      await queryInterface.changeColumn("userGames", "password", {
        type: Sequelize.STRING,
      });
  },
};
