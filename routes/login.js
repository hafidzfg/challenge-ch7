const express = require("express");
const loginController = require("../controllers/authController");
const router = express.Router();

// User login API
router.post("/api/v1/auth/postRegister", loginController.register);

// router.post("/api/v1/login", loginController.loginuser);

router.post("/api/v1/auth/login", loginController.loginjwt);

router.post("/api/v1/adminlogin", loginController.adminlogin);

module.exports = router;
