const express = require("express");
const dashboard = require("../controllers/dashboardController");
const router = express.Router();

router.get("/dashboard", dashboard.display);

// User Game API
router.post("/dashboard/post/user", dashboard.createGame);

router.post("/dashboard/delete/user:user_id", dashboard.deleteGame);

router.post("/dashboard/update/user:user_id", dashboard.updateGame);

// User Biodata API
router.post("/dashboard/post/bio", dashboard.createBio);

router.post("/dashboard/delete/bio:id_user", dashboard.deleteBio);

router.post("/dashboard/update/bio:id_user", dashboard.updateBio);

// User History API
router.post("/dashboard/post/history", dashboard.createHistory);

router.post("/dashboard/delete/history:user_id", dashboard.deleteHistory);

router.post("/dashboard/update/history:user_id", dashboard.updateHistory);

module.exports = router;
