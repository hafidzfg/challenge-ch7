const express = require("express");
const main = require("../controllers/main");
const auth = require("../controllers/authController");
const router = express.Router();
const restrict = require("../middlewares/restrict");

// app.use(router);
// set the view engine to ejs

//set the routers
router.get("/", main.index);

router.get("/login", main.loginpage);

router.get("/game", restrict, main.game);

router.get("/adminlogin", main.adminlogin);

router.get("/whoami", restrict, auth.whoami);

// router.get("/dashboard", function (req, res) {
//   Users.findAll().then(users => {
//       console.log(users)
//       res.render("dashboard", {
//           users
//       })
//   })
// });

module.exports = router;
