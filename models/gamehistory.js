"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class gameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Room, {
        foreignKey: "room_id",
      });
    }
  }
  gameHistory.init(
    {
      hist_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      room_id: DataTypes.INTEGER,
      winner: DataTypes.ARRAY(DataTypes.STRING),
      session: DataTypes.ARRAY(DataTypes.STRING),
      is_active: DataTypes.BOOLEAN,
      playing_date: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "gameHistory",
    }
  );
  return gameHistory;
};
