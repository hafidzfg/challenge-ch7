"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class userHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.userGame, {
        foreignKey: "user_id",
      });
    }
  }
  userHistory.init(
    {
      user_id: DataTypes.STRING,
      date_start: DataTypes.DATE,
      last_played: DataTypes.DATE,
      score: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      modelName: "userHistory",
    }
  );
  return userHistory;
};
