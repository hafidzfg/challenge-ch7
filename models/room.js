"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.userGame, {
        foreignKey: "user_id",
      });
      this.hasOne(models.gameHistory, {
        foreignKey: "room_id",
      });
    }
  }
  Room.init(
    {
      room_id: {
        allowNull: false,
        autoIncrement: true,
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      user_id: DataTypes.STRING,
      members: DataTypes.ARRAY(DataTypes.STRING),
    },
    {
      sequelize,
      modelName: "Room",
    }
  );
  return Room;
};
