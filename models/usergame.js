"use strict";
const { Model } = require("sequelize");

// Import bcrypt for encryption
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class userGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Room, {
        foreignKey: "user_id",
      });
      this.hasOne(models.userBiodata, {
        foreignKey: "user_id",
      });
      this.hasOne(models.userHistory, {
        foreignKey: "user_id",
      });
    }
    // method for encryption
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    // method for register
    static register = ({ user_id, email, password }) => {
      const encryptedPassword = this.#encrypt(password);
      return this.create({ user_id, email, password: encryptedPassword });
    };
    // JWT Strategy
    /* Semua yang berhubungan dengan login */
    // Method untuk melakukan enkripsi
    checkPassword = (password) => bcrypt.compareSync(password, this.password);
    /* Method ini kita pakai untuk membuat JWT */
    generateToken = () => {
      // Jangan memasukkan password ke dalam payload
      const payload = {
        id: this.id,
        user_id: this.user_id,
      };
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const rahasia = "Ini rahasia ga boleh disebar-sebar";
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia);
      return token;
    };

    /* Method Authenticate, untuk login */
    static authenticate = async ({ user_id, password }) => {
      try {
        const user = await this.findOne({ where: { user_id } });
        if (!user) return Promise.reject("User not found!");
        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong password");
        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
      /* Akhir dari semua yang berhubungan dengan login */
    };
  }
  userGame.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        type: DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
        primaryKey: true,
      },
      email: {
        allowNull: false,
        unique: true,
        type: DataTypes.STRING,
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      role: {
        allowNull: false,
        defaultValue: "user",
        type: DataTypes.STRING,
      },
    },
    {
      sequelize,
      modelName: "userGame",
    }
  );
  return userGame;
};
