// lib/passport.js
const passport = require("passport");
// const LocalStrategy = require("passport-local").Strategy;
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { userGame } = require("../models");

// JWT Strategy
/* Passport JWT Options */
const options = {
  // Untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di User Model.
  Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
  secretOrKey: "Ini rahasia ga boleh disebar-sebar",
};
passport.use(
  new JwtStrategy(options, async (payload, done) => {
    // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
    userGame
      .findByPk(payload.user_id)
      .then((user) => done(null, user))
      .catch((err) => done(err, false));
  })
);

// LocalStrategy
// /* Fungsi untuk authentication */
// async function authenticate(user_id, password, done) {
//   try {
//     // Memanggil method kita yang tadi
//     const user = await userGame.authenticate({ user_id, password });
//     /*
// done adalah callback, parameter pertamanya adalah error,
// jika tidak ada error, maka kita beri null saja.
// Parameter keduanya adalah data yang nantinya dapat
// kita akses di dalam req.user */
//     return done(null, user);
//   } catch (err) {
//     /* Parameter ketiga akan dilempar ke dalam flash */
//     return done(null, false, { message: err.message });
//   }
// }
// passport.use(
//   new LocalStrategy(
//     { usernameField: "user_id", passwordField: "password" },
//     authenticate
//   )
// );
// /* Serialize dan Deserialize
// Cara untuk membuat sesi dan menghapus sesi
// */
// passport.serializeUser((user, done) => done(null, user.user_id));
// passport.deserializeUser(async (user_id, done) =>
//   done(null, await userGame.findByPk(user_id))
// );
// Kita exports karena akan kita gunakan sebagai middleware
module.exports = passport;
