const express = require("express");

const app = express();
const session = require("express-session");
const flash = require("express-flash");
const { PORT = 8080 } = process.env;

//use middleware to parse data
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const routes = require("./routes");
const loginAPI = require("./routes/login");
// const loginAPI = require("./controllers/login");
const dashboardAPI = require("./routes/dashboard");
const roomAPI = require("./routes/room");

// setting session handler
app.use(
  session({
    secret: "Make it a secret",
    resave: false,
    saveUninitialized: false,
  })
);

// setting passport
const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

// setting flash
app.use(flash());

// setting view engine
app.set("view engine", "ejs");

// setting router
app.use(loginAPI);
app.use(routes);
app.use(dashboardAPI);
app.use(roomAPI);

// serve the assets, css, and js file
app.use(express.static("public"));

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});
