const express = require("express");
const app = express();

// app.use(router);
// set the view engine to ejs
app.set("view engine", "ejs");

module.exports = {
  index: (req, res) => {
    res.render("index.ejs");
  },
  loginpage: (req, res) => {
    res.render("loginpage.ejs");
  },
  game: (req, res) => {
    res.render("game.ejs");
  },
  adminlogin: (req, res) => {
    res.render("adminlogin.ejs");
  },
};
