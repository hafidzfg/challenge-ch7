const express = require("express");
const bodyParser = require("body-parser");
const router = express();
// const fs = require("fs");
let userlogin = require("../db/admin.json");
const { userBiodata, userGame, userHistory } = require("../models");
// const passport = require("../lib/passport");

function format(user) {
  const { id, user_id } = user;
  return {
    id,
    user_id,
    accessToken: user.generateToken(),
  };
}

//configure body parser middleware
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

module.exports = {
  // User login API
  register: (req, res, next) => {
    userGame
      .register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },

  // // LocalStrategy
  // loginuser: passport.authenticate("local", {
  //   successRedirect: "/whoami",
  //   failureRedirect: "/login",
  //   failureFlash: true, // Untuk mengaktifkan express flash
  // }),

  // JWT Strategy
  loginjwt: (req, res) => {
    userGame.authenticate(req.body).then((user) => {
      res.json(format(user));
    });
  },

  adminlogin: (req, res) => {
    const login = req.body;
    console.log(userlogin);
    for (let i = 0; i < userlogin.length; i++) {
      const element = userlogin[i];
      if (
        login.username === element.username &&
        login.password === element.password
      ) {
        res.status(200);
        res.redirect("/dashboard");
      } else if (
        login.username != element.username ||
        login.password != element.password
      ) {
        res.status(401);
        res.send("Wrong email or password!");
      } else if (err)
        res.send("Something is wrong! Please contact the developer");
    }
  },
  whoami: (req, res) => {
    const currentUser = req.user.user_id;
    res.json(currentUser);
  },
};
