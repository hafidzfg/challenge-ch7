const express = require("express");
const router = express();
const { userBiodata, userGame, userHistory } = require("../models");

// MENAMBAHKAN DEPENDENCY SEQUELIZE
// MEMBUAT MODEL, MIGRATION

router.use(express.Router());
router.use(express.json());

module.exports = {
  // buatlah endpoint controller untuk mengakses dashboard
  display: (req, res, next) => {
    // MENAMBAHKAN KODE UNTUK MENGAMBIL DATA LIST USER DI DATABASE
    Promise.all([
      userGame.findAll(),
      userBiodata.findAll(),
      userHistory.findAll(),
    ]).then((data) => {
      res.render("dashboard", {
        usergames: data[0],
        users: data[1],
        userhistory: data[2],
      });
    });
  },

  // User Game API
  createGame: (req, res, next) => {
    userGame
      .create({
        user_id: req.body.user_id,
        email: req.body.email,
        password: req.body.password,
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  deleteGame: (req, res, next) => {
    userGame
      .destroy({
        where: {
          user_id: req.body.user_id,
        },
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  updateGame: (req, res, next) => {
    userGame
      .findOne({
        where: {
          user_id: req.body.user_id,
        },
      })
      .then((data) => {
        data
          .update({
            email: req.body.email,
            password: req.body.password,
            role: req.body.role,
          })
          .then(() => {
            res.redirect("/dashboard");
          });
      });
  },

  // User Biodata API
  createBio: async (req, res, next) => {
    await userBiodata
      .create({
        id_user: req.body.id_user,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        address: req.body.address,
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  deleteBio: async (req, res, next) => {
    await userBiodata
      .destroy({
        where: {
          id_user: req.body.id_user,
        },
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  updateBio: (req, res, next) => {
    userBiodata
      .findOne({
        where: {
          id_user: req.body.id_user,
        },
      })
      .then((data) => {
        data
          .update({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            address: req.body.address,
          })
          .then(() => {
            res.redirect("/dashboard");
          });
      });
  },

  // User History API
  createHistory: async (req, res, next) => {
    await userHistory
      .create({
        user_id: req.body.user_id,
        score: req.body.score,
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  deleteHistory: (req, res, next) => {
    userHistory
      .destroy({
        where: {
          user_id: req.body.user_id,
        },
      })
      .then(() => {
        res.redirect("/dashboard");
      });
  },

  updateHistory: (req, res, next) => {
    userHistory
      .findOne({
        where: {
          user_id: req.body.user_id,
        },
      })
      .then((data) => {
        data
          .update({
            score: req.body.score,
          })
          .then(() => {
            res.redirect("/dashboard");
          });
      });
  },
};
